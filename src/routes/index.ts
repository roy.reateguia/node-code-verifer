/**
 * Root Router
 * Redirect to  Routers
*/
import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../utils/logger';

// Intancia del Server
let server = express();
// Router Instance 
let rootRouter = express.Router();

// Active for requests to http://localhost:8000/api

// GET:  http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response)=> {
    LogInfo('GET: http://localhost:8000/api/ ');
    res.send('Welcome to GET Route:hello')
})

// Redireccion to routes & controller
server.use('/', rootRouter) // http://localhost:8000/api/ 
server.use('/hello', helloRouter) // http://localhost:8000/api/hello --> HelloRouter
// Add more routes to the app

export default server



