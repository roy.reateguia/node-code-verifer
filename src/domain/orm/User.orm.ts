import { userEntity } from "../Entities/User.entity";
import { LogError, LogSuccess } from "@/utils/logger";

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const GetAllUser = async(): Promise<any[] | undefined > =>{    
    try {
        let userModel = userEntity()

        //Search alll users 
        return await userModel.find({isDelete:false})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error} `)
    }
}


// TODO:

// - Get User By ID
// - Get User By Email
// - Delete User By Id
// - Create New User
// - Update User By Id
