import dotenv from 'dotenv'
import server from './src/server';
import { LogError, LogSuccess } from './src/utils/logger';
// Configuration the .env file
dotenv.config();

const port = process.env.PORT || 8000;

// * Execute Server
server.listen(port, () => {
    LogSuccess(`[Server ON]: Running inhttp://localhost:${port}/api`)
})


// * Control SERVER ERROR
server.on('error', (error) =>{
    LogError(`[SERVER ERROR]: ${error}`)
})

